# GitLab DevSecOps Basics

This tutorial contains lessons for learning how to leveraging DevSecOps within GitLab. It highlights the key benefits to using the service and will get you up and running with DevSecOps in no time. Each Lab will have a video walk-through the steps.

![]() <- Insert Video

## Target Audience

The intended audience is:

- Developers who want to learn more about becoming more Security focused
- Developers who wish to collaborate more with AppSec Engineers
- AppSec Engineers wishing to learn how manage vulnerabilities
- AppSec Engineers wanting to learn how to safeguard their applications

## Labs

Here are some interactive labs that you can go through.

- [Pre-Requisites](./docs/01-prerequisites.md)
- [Deploying Application with Pipeline](./docs/02-deploying-application-with-pipeline.md)
- [Enabling Security Scans](./docs/03-enabling-security-scans.md)
- [Viewing Vulnerabilities](./docs/04-viewing-vulnerabilities.md)
- [Vulnerability Management](./docs/05-vulnerability-management.md)
