# Viewing Vulnerabilities in the MR

In this lab, we will go over how Vulnerabilities can be viewed as well as the information and actions available to a user. We are going to add some vulnerable code to a feature branch and then the scanners will run and display the found vulnerabilities.

![]() <- Insert Video

## Step 1: Adding vulnerable code

Now let's go ahead and add some vulnerabilities. We will make sure that something can be picked up by each type of scanner.

1. Open the **WebIDE**

![](../images/v1.png) 

2. Open `notes/db.py` and add the following under `conn = sqlite3.connect(name)`. This is done to give the database file global permissions, which is a security issue
```
os.chmod(name, 777)
```

3. Open `notes/routes.py` and add to the end of the file. This will add a new route that can be accessed at the /get-with-vuln URI path that allows us to test DAST in this lab scenario
```
@note.route('/get-with-vuln', methods=['GET'])
def get_note_with_vulnerability():
    id = request.args.get('id')
    conn = db.create_connection()

    with conn:
        try:
            return str(db.select_note_by_id(conn, id))
        except Exception as e:
            return "Failed to delete Note: %s" % e
```

4. Create a file in `chart/templates` called `vulns.yaml` and add the following:
```
apiVersion: v1
kind: Pod
metadata:
    name: kubesec-demo
spec:
    containers:
    - name: kubesec-demo
      image: gcr.io/google-samples/node-hello:1.0
      securityContext:
        readOnlyRootFilesystem: true
```

5. Open `requirements.txt` and change the content to the following:
```
Flask==0.12.2
django==2.0.0
flask_wtf
wtforms
flask-bootstrap
pysqlite3
dubbo-client
``` 

6. Open `Dockerfile` and change the alpine version to the following:
```
FROM python:alpine3.7
```

7. Review your code changes, and make sure they match the instructions above

8. Select **Commit**

![](../images/v2.png)

9. Select **Create a new branch** and give that branch a name, then select **Start a new merge request**, and press **Commit**

![](../images/v3.png)

10. Create the Merge Request and press **Submit merge request**

![](../images/v4.png)

A new pipeline will now run against the branch you created

![](.../images/v5.png)

## Step 2: Viewing Vulnerable Code

Now we can view the vulnerabilities after the pipeline started above
has completely run. Let's dig into the vulnerabilities and perform some actions on them.

1. Wait for the pipeline to complete, this may take about 3 minutes. Refresh the page to be able to see the security scan report

2. Within the merge request, press **Expand** on the Security Scans

![](../images/v6.png)

3. You should see something like this

![](../images/v7.png)

4. Click on the **Chmod setting a permissive mask 0o1411 on file (name)** vulnerability
  
![](../images/v8.png)

5. You should see a pop-up as follows:

![](../images/v9.png)

6. Dismiss the Vulnerability by clicking **Dismiss vulnerability**

![](../images/v10.png)

7. You can see it has been crossed out

![](../images/v11.png)

8. Click on the same vulnerability

![](../images/v12.png)

9. You should see a pop-up as follows:

![](../images/v13.png)

10. Click on **Create issue**

![](../images/v14.png)

11. You should now see something like this

![](../images/v15.png)

Now let's go back to the Merge Request by pressing the back button on your browser.

## Step 3: Viewing Denied Licenses

Within the same MR view, we can see the licenses that were detected. You'll be able to see which licenses are approved and denied according to the policy we set in an earlier lab.

1. Within the merge request expand the **license** section

![](../images/v16.png)

2. See that the **Apache License 2.0** has been denied

![](../images/v17.png)

## Step 4: Merging the Code

We can now merge the code. This is done so that the Vulnerability Report can be populated with this data.

1. Click **view eligible approvers**

![](../images/v18.png)

2. You should see that the merge request approvals are active

![](../images/v19.png)

3. Press **Merge**

![](../images/v20.png)

4. the pipeline should now be running for the master branch

![](../images/v21.png)

---

Congratulations! You have now successfully viewed vulnerabilities within an MR and the details to their resolution. Now we can move on to [Vulnerability Management](./05-vulnerability-management.md)

