# Pre-requisites

In order to get started with GitLab DevSecOps, you will need the following:

- A GitLab account
- A License for GitLab Ultimate
- Google Cloud SDK
- Kubectl
- A GKE Cluster

![]() <- Insert Video

## GitLab Account

You may already have a GitLab account, if not you can register here:
https://gitlab.com/users/sign_up

## GitLab Ultimate

In order to get the most out of GitLab DevSecOps, you will require GitLab
Ultimate. If you do not have GitLab Ultimate, you can sign-up for a 30-day trial
license here: https://about.gitlab.com/free-trial/

## Google Cloud SDK

Before starting, you should download and install the Google Cloud SDK. It will allow us to interact with set our cluster via the command-line.

Download it from here: https://cloud.google.com/sdk/docs/quickstart

## KubeCtl

The Kubernetes command-line tool, kubectl, allows you to run commands against Kubernetes clusters. We will need it to interact with the cluster we have created via the CLI.

You can download it from here: https://kubernetes.io/docs/tasks/tools/

## Kubernetes Cluster (GKE)

In this particular tutorial, we will be using a GKE cluster. You can get
$300 towards a GKE cluster when you first sign-up here: https://cloud.google.com/kubernetes-engine

Once you have access to the Google cloud console, you can create a cluster as follows:

1. Go to the Kubernetes Engine Menu in Google Cloud Platform

![](../images/c1.png)

2. Click on the **Create** button

![](../images/c2.png)

3. Click on the **Configure** button under Standard

![](../images/c3.png)

4. Give the cluster a name

![](../images/c4.png)

5. In the **Nodes** menu, select the **Series** and a **Machine type** and then
press the **Create** button

![](../images/c5.png)

**Note:** I selected the e2-micro (2 vCPU, 1GB memory) machine, since this workshop
doesn't require anything more than that.

6. Click in the rendering cluster

![](../images/cr1.png)

7. Wait a few minutes, and the cluster should be ready

![](../images/c6.png)

8. Save the following information for later use

- CA certificate
- Endpoint

![](../images/cr2.png)

9. Click on Connect and copy the Command-Line access command

![](../images/cr3.png)

10. Paste the command in the terminal

![](../images/cr4.png)

11. Now let's create a GitLab-Service Account needed for adding the cluster
to our project. Run the following command:

```
$ echo '
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system
' | kubectl apply -f -

serviceaccount/gitlab created
clusterrolebinding.rbac.authorization.k8s.io/gitlab-admin created
```

10. Now let's grab the service-token and save it for later use

```
# Find the Secret Created for the GitLab Admin
$ kubectl get secrets -n kube-system | grep gitlab

gitlab-token-rqccb

# Grab the token from the Kube Secret
$ kubectl get secret -n kube-system gitlab-token-rqccb -o yaml | grep token

token: ZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNklqWTNOMVUyUkc1VmFtRktZVEV4VlRobVVYYzRlbEp2ZVhCNk5FUmxOMFJVU0RsSGQxaG9UWE5IV0RRaWZRLmV5SnBjM01pT2lKcmRXSmxjbTVsZEdWekwzTmxjblpwWTJWaFkyTnZkVzUwSWl3aWEzVmlaWEp1WlhSbGN5NXBieTl6WlhKMmFXTmxZV05qYjNWdWRDOXVZVzFsYzNCaFkyVWlPaUpyZFdKbExYTjVjM1JsYlNJc0ltdDFZbVZ5Ym1WMFpYTXVhVzh2YzJWeWRtbGpaV0ZqWTI5MWJuUXZjMlZqY21WMExtNWhiV1VpT2lKbmFYUnNZV0l0ZEc5clpXNHRjbkZqWTJJaUxDSnJkV0psY201bGRHVnpMbWx2TDNObGNuWnBZMlZoWTJOdmRXNTBMM05sY25acFkyVXRZV05qYjNWdWRDNXVZVzFsSWpvaVoybDBiR0ZpSWl3aWEzVmlaWEp1WlhSbGN5NXBieTl6WlhKMmFXTmxZV05qYjNWdWRDOXpaWEoyYVdObExXRmpZMjkxYm5RdWRXbGtJam9pTTJZM1pqWXdORGd0TWpreFpDMDBORGRrTFdFNE9UVXRObVJoWXpBMk5UbG1aamRqSWl3aWMzVmlJam9pYzNsemRHVnRPbk5sY25acFkyVmhZMk52ZFc1ME9tdDFZbVV0YzNsemRHVnRPbWRwZEd4aFlpSjkubmp4bWFJUEx5YkkzTjNES0h3WVlBSUQ5QWNlY1NnYkFadjhzYUs4VUkwNEV2cVh2M1Y2VGx2X0M0MVl6N3JjRHJRZ29sRUR5Z3FPMjNhVDRXcksyMGxWdW5OZXltRW11eWFtdE5qc1QwcHhXMkpVX0RXWjFxTjhsMlRqMEpzWnFBOEZkQXN1NnVhLUVmOWdteC1SMEdVWkN4cVRlbExzcGZpaFo5NTRzbGNpZVRyVjVtU0lLSV9FdVdDSWNjNHd2SlJfc2F3SktjTjBKR2xld3NiSk4yV0pHaWVGZUNfQXkyWVM4YlZBbG5MQlJmb3I3SC1MYmJzelJuYVdqUUdSaWFIaDZ3WGYtU1IwQWxFWlZVVlA3RWZhck4xNGpUVk9ZNTd2cXNZSmsxVEhoMW5uR1pOLUN3MjBMeEE5OVFMcUF6M2ZoQ3NCVXktWHFoYXJwOG1HZURB

# Encode the Token in Base64
$ echo "ZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNklqWTNOMVUyUkc1VmFtRktZVEV4VlRobVVYYzRlbEp2ZVhCNk5FUmxOMFJVU0RsSGQxaG9UWE5IV0RRaWZRLmV5SnBjM01pT2lKcmRXSmxjbTVsZEdWekwzTmxjblpwWTJWaFkyTnZkVzUwSWl3aWEzVmlaWEp1WlhSbGN5NXBieTl6WlhKMmFXTmxZV05qYjNWdWRDOXVZVzFsYzNCaFkyVWlPaUpyZFdKbExYTjVjM1JsYlNJc0ltdDFZbVZ5Ym1WMFpYTXVhVzh2YzJWeWRtbGpaV0ZqWTI5MWJuUXZjMlZqY21WMExtNWhiV1VpT2lKbmFYUnNZV0l0ZEc5clpXNHRjbkZqWTJJaUxDSnJkV0psY201bGRHVnpMbWx2TDNObGNuWnBZMlZoWTJOdmRXNTBMM05sY25acFkyVXRZV05qYjNWdWRDNXVZVzFsSWpvaVoybDBiR0ZpSWl3aWEzVmlaWEp1WlhSbGN5NXBieTl6WlhKMmFXTmxZV05qYjNWdWRDOXpaWEoyYVdObExXRmpZMjkxYm5RdWRXbGtJam9pTTJZM1pqWXdORGd0TWpreFpDMDBORGRrTFdFNE9UVXRObVJoWXpBMk5UbG1aamRqSWl3aWMzVmlJam9pYzNsemRHVnRPbk5sY25acFkyVmhZMk52ZFc1ME9tdDFZbVV0YzNsemRHVnRPbWRwZEd4aFlpSjkubmp4bWFJUEx5YkkzTjNES0h3WVlBSUQ5QWNlY1NnYkFadjhzYUs4VUkwNEV2cVh2M1Y2VGx2X0M0MVl6N3JjRHJRZ29sRUR5Z3FPMjNhVDRXcksyMGxWdW5OZXltRW11eWFtdE5qc1QwcHhXMkpVX0RXWjFxTjhsMlRqMEpzWnFBOEZkQXN1NnVhLUVmOWdteC1SMEdVWkN4cVRlbExzcGZpaFo5NTRzbGNpZVRyVjVtU0lLSV9FdVdDSWNjNHd2SlJfc2F3SktjTjBKR2xld3NiSk4yV0pHaWVGZUNfQXkyWVM4YlZBbG5MQlJmb3I3SC1MYmJzelJuYVdqUUdSaWFIaDZ3WGYtU1IwQWxFWlZVVlA3RWZhck4xNGpUVk9ZNTd2cXNZSmsxVEhoMW5uR1pOLUN3MjBMeEE5OVFMcUF6M2ZoQ3NCVXktWHFoYXJwOG1HZURB" | base64 -d

eyJhbGciOiJSUzI1NiIsImtpZCI6IjY3N1U2RG5VamFKYTExVThmUXc4elJveXB6NERlN0RUSDlHd1hoTXNHWDQifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJnaXRsYWItdG9rZW4tcnFjY2IiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZ2l0bGFiIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiM2Y3ZjYwNDgtMjkxZC00NDdkLWE4OTUtNmRhYzA2NTlmZjdjIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmUtc3lzdGVtOmdpdGxhYiJ9.njxmaIPLybI3N3DKHwYYAID9AcecSgbAZv8saK8UI04EvqXv3V6Tlv_C41Yz7rcDrQgolEDygqO23aT4WrK20lVunNeymEmuyamtNjsT0pxW2JU_DWZ1qN8l2Tj0JsZqA8FdAsu6ua-Ef9gmx-R0GUZCxqTelLspfihZ954slcieTrV5mSIKI_EuWCIcc4wvJR_sawJKcN0JGlewsbJN2WJGieFeC_Ay2YS8bVAlnLBRfor7H-LbbszRnaWjQGRiaHh6wXf-SR0AlEZVUVP7EfarN14jTVOY57vqsYJk1THh1nnGZN-Cw20LxA99QLqAz3fhCsBUy-Xqharp8mGeDA
```

11. Make sure you have the following saved:

- Cluster Endpoint
- Cluster Certificate
- Service Token

---

Congratulations! You have just met all the prerequisites and created a Kubernetes Cluster. Now we can move on to [Deploying an Application](./02-deploying-application-with-pipeline.md)
