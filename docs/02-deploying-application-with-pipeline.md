# Deploying an Application with GitLab Pipeline

In this lesson, we will clone the project over to our space, so that we can make
edits. We will deploy our application to our Kubernetes cluster. This will allow us to access the application from the outside world. Make sure you are signed in to GitLab before starting this tutorial.

![]() <- Insert Video

## Step 1: Cloning the Sample Project

Here we will clone the sample project which we will use through this workshop. It's a simple python Flask application which add/removes notes from a SQLite DataBase.

1. From the GitLab UI, Select **New Project**

![](../images/1.png)

2. Select **Import project/repository**

![](../images/2.png)

3. Select **Repo By URL**

![](../images/3.png)

4. Enter `https://gitlab.com/tech-marketing/workshops/devsecops/simply-simple-notes.git` as the **Git repository url**

![](../images/4.png)

5. Give the Project a name and then press the **Create Project** button

![](../images/5.png)

## Step 2. Setting up the Project to use the Cluster

We will now add the Kubernetes cluster we created in the prerequisites section to the project. This allows our application to be deployed to our cluster.

1. On the sidebar, select **Operations > Kubernetes**

![](../images/k1.png)

2. Press the **Create Cluster with Certificate** button

![](../images/k2.png)

3. Select **Connect existing cluster**

![](../images/k3.png)

4. Add a **Kubernetes cluster name**, the **API URL**, the **CA Certificate**, and the **Service Token**

![](../images/k4.png)

**Note: There is information on where to find these in the prerequisite section, also make sure the API URL has 'https://' included**

5.Make sure **RBAC-enabled cluster**, **GitLab-managed cluster**, and **Namespace per environment** are checked. Then click the **Add Kubernetes cluster** button

![](../images/k5.png)

## Step 3: Installing the Ingress Controller

We will install the Ingress-Controller so that our application can be exposed to the outside world. That way we can access it from our browser.

1. On the sidebar, select **Infrastructure > Kubernetes**

![](../images/b1.png)

2. Click on the cluster we just added

![](../images/b2.png)

3. Click the **Advanced Settings** tab

![](../images/b3.png)

4. Under **Cluster management project** select this project and press **Save Changes**

![](../images/b4.png)

5. Go back to the project home

6. Click on the Web IDE

![](../images/b5.png)

7. Create a folder in the root directory

![](../images/b6.png)

8. Name it **applications** and press **Create Directory**

![](../images/b7.png)

9. Under **applications** create a folder

![](../images/b8.png)

10. Name it **ingress** and press **Create Directory**

![](../images/b9.png)

11. Create a file in the **ingress** directory

![](../images/b10.png)

12. Name it helmfile.yaml and press **Create File**

![](../images/b11.png)

13. Add the following to it
```
repositories:
  - name: stable
    url: https://charts.helm.sh/stable

releases:
- name: ingress
  namespace: gitlab-managed-apps
  chart: stable/nginx-ingress
  version: 1.40.2
  installed: true
  values:
    - values.yaml
```

14. Create a file in the **ingress** directory

![](../images/b12.png)

15. Name it values.yaml and press **Create File**

![](../images/b13.png)

16. Add the following to it
```
controller:
  image:
    repository: "quay.io/kubernetes-ingress-controller/nginx-ingress-controller"
  stats:
    enabled: true
  podAnnotations:
    prometheus.io/scrape: "true"
    prometheus.io/port: "10254"
```

17. Create a file in the root directory

![](../images/b14.png)

18. Name it **helmfile.yaml** and press **Create File**

# ![](../images/b15.png)

19. Add the following to it:
```
helmDefaults:
  atomic: true
  wait: true

helmfiles:
    - path: applications/ingress/helmfile.yaml
```

21. Open the `.gitlab-ci.yaml` file

![](../images/b16.png)

22. Add the following to the stages
```
stages:
  - build
  - apply
  - staging
```

23. Add the following job
```
apply:
  stage: apply
  image: "registry.gitlab.com/gitlab-org/cluster-integration/cluster-applications:v1.1.0"
  environment:
    name: staging
  script:
    - gl-ensure-namespace gitlab-managed-apps
    - gl-helmfile --file $CI_PROJECT_DIR/helmfile.yaml apply --suppress-secrets
```

24. Press **Commit**:

![](../images/s3.png)

25. Select **Commit to master branch** and press **Commit**:

![](../images/s4.png)

## Step 4: Running the Pipeline

Now let's run a pipeline to deploy the application to our Kubernetes cluster.

1. Click on the **CI/CD** left navigation menu and click on **Pipelines**

![](../images/p1.png)

2. Click on **Run Pipeline**

![](../images/p2.png)

3. Ensure that the **master** branch is selected

![](../images/p3.png)

4. You should now see the pipeline running the build stage

![](../images/p4.png)

## Step 5: Accessing our application

1. Once the pipeline seen above completes, click on the `deploy_staging` job:

![](../images/p5.png)

2. Search for `KUBE_INGRESS_BASE_DOMAIN`:

![](../images/p6.png)

3. Point your browser to  `KUBE_INGRESS_BASE_DOMAIN` and add **/notes** to the URL. The image below shows an example:

![](../images/ka4.png)

---

Congratulations! You have now successfully deployed an application using GitLab CICD. Now we can move on to [Enabling Security Scans](./03-enabling-security-scans.md)