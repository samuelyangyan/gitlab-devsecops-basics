# Enabling Security Scans

In this section, we will go over the security scans which GitLab offers. We will then setup all of the scans and run them on our master branch.

![]() <- Insert Video

## What Security Scans does GitLab offer

GitLab offers a variety of security scans to enhance application security. Some scanners will scan the static source code, and others will scan the running application for vulnerabilities.

The scanners use OpenSource tools, which vary by language. The language in the application is auto-detected by GitLab. For these OpenSource tools, the infrastructure is maintained by GitLab, saving users lot's of cost and hassle maintaining.

We will go over the following scanners:

1. [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/): analyzes your source code for known vulnerabilities.
2. [Dynamic Application Security Testing (DAST)](https://docs.gitlab.com/ee/user/application_security/dast/): analyzes your running application for known vulnerabilities.
3. [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/): scans Docker images for  known vulnerabilities
4. [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/): scans project dependencies for known vulnerabilities
5. [License Scanning](https://docs.gitlab.com/ee/user/compliance/license_compliance/): scans licenses to see if they are incompatible with a set policy
6. [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/): Scans for secrets checked into source code

## Step 1: Adding Security Scans to the pipeline

Now let's go ahead and make sure that Security Scans run within our Pipeline. This means that anytime there is any code change on any branch, the pipeline will run the security scans.

1. Open the **WebIDE**  

![](../images/s1.png)

2. Select `.gitlab-ci.yml`  

![](../images/s2.png)

3. Add Security Scan templates to the yaml file using **include** block that you will find in top of the file.
```
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/DAST.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
```

**Note: These jobs will automatically pickup the programming language or framework**

4. Add Kubernetes scanning configuration for SAST by adding the `SCAN_KUBERNETES_MANIFESTS` to the **variables**:
```
variables:
  ...
  SCAN_KUBERNETES_MANIFESTS: "true"
  ...
```  

5. Let DAST know where to scan for the application by adding the `DAST_WEBSITE` to the **variables**, this will be the URL used to access the application
```
variables:
  ...
  DAST_WEBSITE: http://KUBE_INGRESS_BASE_DOMAIN/notes
  ...
```

6. Add `test` and `dast` to **stages** so it would look as follows:
```
stages:
  - build
  - test
  - staging
  - dast
```
    
7. Press **Commit**:

![](../images/s3.png)

8. Select **Commit to master branch** and press **Commit**:

![](../images/s4.png)

9. Now lets go back to the project's main page.

10. You can see that a pipeline is running

![](../images/s5.png)

11. Once you click on the pipeline icon, you should see the following

![](../images/s6.png)

## Step 2: Setting up Merge-Request Approvals

Code review is an essential practice of every successful project. Approving a merge request is an important part of the review process, as it clearly communicates the ability to merge the change.

GitLab provides Security guard-rails to prevent vulnerable code
from being merged without approval. This includes vulnerabilities as well as incompatible licenses. Now let's setup these guardrails, known as merge-request approvals.

1. Go to the the **Settings** left navigation menu and press **General**:  

![](../images/ma1.png)

2. Expand **Merge request approvals**   

![](../images/ma2.png)

3. Click on **Enable** for **Vulnerability-Check**  

![](../images/ma3.png)

You should then see a popup as follows

![](../images/ma4.png)

4. Type in your username in the **Approvers** dropdown

![](../images/ma5.png)

5. Press **Add approval rule**

![](../images/ma6.png)

6. Confirm the Rule was added  

![](../images/ma7.png)

7. Do the same for **License-Check**. Once complete, you should see the following

![](../images/ma8.png)

## Step 3: Creating a License Policy

License Policies allow you to declare licenses as either approved or denied.
When Merge-Request Approvals are set up a denied license will block an MR from
being merged.

1. Under the **Security & Compliance** left navigation menu, go to **License Compliance**

![](../images/l1.png)

2. Click on **Policies**

![](../images/l2.png)

3. Click on **Add license and related policy**

![](../images/l3.png)

4. Type in **Apache License 2.0**, select **Deny**, and press **Submit**

![](../images/l4.png)

5. You should now see the following as confirmation

![](../images/l5.png)

---

Congratulations! You have now successfully run Security Scans and enabled DevSecOps for your application. Now we can move on to [Viewing Vulnerabilities in the MR](./04-viewing-vulnerabilities.md)
